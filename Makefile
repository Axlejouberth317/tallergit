CC=gcc
CFLAGS=-c -I.
DEPS=point.h

all: distancia

distancia: main.o point.o point.h
	$(CC) -o distancia main.o point.o -I. -lm

main.o: main.c
	$(CC) $(CFLAGS) main.c

point.o: point.c
	$(CC) $(CFLAGS) point.c

 
clean:
	rm *o distancia
	
